package com.dscheck.androidtestinjection;

import android.content.Context;
import android.util.Log;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDataRetriever {
    @Mock
    DataRetriever mMockData;

    int mIteration;


    private int getValue(DataRetriever data, int numIterations) {
        int value = -1;
        for(int i = 0; i < numIterations; i++)  {
            value = data.getNext();
        }
        return value;
    }

    @Test
    public void test_one_get() throws Exception {
        DataRetriever data = new DataRetriever();
        int value = getValue(data, 1);
        assertTrue(value == 1);
    }


    @Test
    public void test_two_gets() throws Exception {
        DataRetriever data = new DataRetriever();
        int value = getValue(data, 2);
        assertTrue(value == 2);
    }
    @Test
    public void test_three_gets() throws Exception {
        DataRetriever data = new DataRetriever();
        int value = getValue(data, 3);
        assertTrue(value == 3);
    }

    @Test
    public void test_mock_get() throws Exception {
        DataRetriever data = Mockito.mock(DataRetriever.class);
        final int index = 0;
        when(data.getNext()).thenAnswer(new Answer<Integer>() {
            @Override
            public Integer answer(InvocationOnMock invocation) throws Throwable {
                return getMockedValue();
            }
        });
        mIteration = 0;

        int value = getValue(data, 1);
        try {
            assertTrue("First", value == 0);
        } catch (AssertionError e) {
            System.out.println("Failed " + value);
            throw e;
        }
        mIteration++;

        value = getValue(data, 1);
        assertTrue("Second", value == 5);
        mIteration++;
    }

    private Integer getMockedValue() {
        return 5 * mIteration;
    }

}