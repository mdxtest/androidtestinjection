package com.dscheck.androidtestinjection;

import android.app.Application;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.TextView;

import com.google.inject.AbstractModule;
import com.google.inject.util.Modules;
import com.robotium.solo.Solo;

import roboguice.RoboGuice;

/**
 * Created by dscheck on 7/26/2016.
 */
public class MainActivityTestBase extends ActivityInstrumentationTestCase2<MainActivity> {
    protected Solo mSolo;

    public class MockedDataRetriever extends DataRetriever {
        @Override
        protected int calculateNext() {
            mValue += 5;
            return mValue;
        }
    }

    protected MockedDataRetriever mMockedDataRetriever = new MockedDataRetriever();

    private class MockedDataRetrieverModule extends AbstractModule {
        @Override
        protected void configure() {
            bind(DataRetriever.class).toInstance(mMockedDataRetriever);
        }
    }

    public MainActivityTestBase() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        Application app = (Application) getInstrumentation()
                .getTargetContext().getApplicationContext();
        RoboGuice.setupBaseApplicationInjector(
                app, RoboGuice.DEFAULT_STAGE,
                Modules.override(RoboGuice.newDefaultRoboModule(app))
                        .with(new MockedDataRetrieverModule()));

        mSolo = new Solo(getInstrumentation(), getActivity());
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        RoboGuice.Util.reset();
    }

    protected void clickButtonExpectValue(int value) {
        View button = mSolo.getView(R.id.incrButton);
        mSolo.clickOnView(button);
        TextView valueText = (TextView) mSolo.getView(R.id.valueText);
        assertTrue(valueText.getText().equals(Integer.toString(value)));
    }
}