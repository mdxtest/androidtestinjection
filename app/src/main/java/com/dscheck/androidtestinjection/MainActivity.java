package com.dscheck.androidtestinjection;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView mValueText;
    Button mButton;

    DataRetriever mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mValueText = (TextView) findViewById(R.id.valueText);
        mButton = (Button) findViewById(R.id.incrButton);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                incrClick();
            }
        });

        mData = new DataRetriever();

    }

    private void incrClick() {
        int value = mData.getNext();
        mValueText.setText(Integer.toString(value));
    }
}
