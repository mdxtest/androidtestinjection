package com.dscheck.androidtestinjection;

/**
 * Created by dscheck on 7/26/2016.
 */
public class DataRetriever {
    protected int mValue = 0;

    protected int getNext() {
        return calculateNext();
    }

    protected int calculateNext() {
        return ++mValue;
    }

}
